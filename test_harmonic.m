% TEST_HARMONIC
% Generates Figure 5.3

clear all;
close all;

b     = 32;
n     = 1e8;
npart = ceil(n/b);

for t = [11 8 4]
  strue = zeros(npart+1,1);
  s1    = zeros(npart+1,1);
  s2    = zeros(npart+1,1);
  s3    = zeros(npart+1,1);
  x     = ones(n,1)./(1:n)'; 
  x     = chop(x,t); 
  e     = 0;
  for j = 2:npart+1
    ibeg = 1+(j-1)*b;
    iend = min(j*b,n);
    strue(j) = strue(j-1) + sum(x(ibeg:iend));
    sloc = 0;
    for i = ibeg:iend
      sloc = chop(sloc + x(i), t);
    end
    s1(j) = chop(s1(j-1) + sloc, t);
    y     = chop(e + sloc, t);
    s2(j) = chop(s2(j-1) + y, t);
    e     = chop(chop(s2(j-1) - s2(j), t) + y, t); 
    s3(j) = s3(j-1) + sloc;
  end
  
  fs=14; ms=7; lw=1.5;
  figure()
  I = round(logspace(0,log10(npart),50));
  semilogx(I*b,s1(I+1),'-o','Markersize',ms,'LineWidth',lw);
  hold on;
  semilogx(I*b,s2(I+1),'-x','Markersize',ms,'LineWidth',lw);
  semilogx(I*b,s3(I+1),'-v','Markersize',ms,'LineWidth',lw);
  semilogx(I*b,strue(I+1),'-s','Markersize',ms,'LineWidth',lw);
  hleg = legend('Blocked', 'FAB compensated', 'FAB extended $$(u_e=2^{-53})$$)', 'True');
  set(hleg,'Location','NorthWest','Fontsize',fs);
  set(hleg,'interpreter','latex');
  xlabel('$$n$$','interpreter','latex','fontsize',fs);
  xlim([b n]);
  ylabel('$$s$$','interpreter','latex','fontsize',fs,'rotation',0);
  set(gca, 'Fontsize', fs);
  set(gcf, 'Color', 'w');
end
