% SUMMATION_CHOP
% Computes sum(x) with several summation algorithms in any precision 2^-t
% Inputs:
%    x    = input vector, expected in double precision
%    t    = precision used (unit roundoff u=2^-t)
%    summ = summation algorithm to be used. Possible values:
%           'rec'     => recursive summation
%           'blk'     => blocked summation
%           'comp'    => Kahan's compensated summation
%           'FABcomp' => FABsum algorithm with compensated summation as AccurateSum
%           'FABext'  => FABsum algorithm with extended (single) precision summation as AccurateSum
%    b    = block size (optional argument for FABsum)
% Outputs:
%    s    = computed sum
function s = summation_chop(x,t,summ,b)
  n = length(x);
  switch(summ)
  case 'rec'
    s = 0;
    for i=1:n
      s = chop(s + x(i), t);
    end
  case 'comp'
    s = 0; e = 0;
    for i=1:n
      temp = s;
      y    = chop(e + x(i), t);
      s    = chop(temp + y, t);
      e    = chop(chop(temp - s, t) + y, t);
    end
  case 'blk'
    npart = ceil(n/b);
    s = 0; 
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      s = chop(s + summation_chop(x(ibeg:iend), t, 'rec'), t);
    end
  case 'FABcomp'
    npart = ceil(n/b);
    s = 0; e = 0;
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      temp = s;
      y    = chop(e + summation_chop(x(ibeg:iend), t, 'rec'), t);
      s    = chop(temp + y, t);
      e    = chop(chop(temp - s, t) + y, t);
    end
  case 'FABext'
    npart = ceil(n/b);
    s = 0; 
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      s    = chop(s + summation_chop(x(ibeg:iend), t, 'rec'), 24);
    end
    s = chop(s,t);
  otherwise
    error('Wrong summ argument\n')
  end
end
