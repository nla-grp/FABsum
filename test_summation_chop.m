% TEST_SUMMATION_CHOP
% Generates Figure 5.2

clear all;
close all;

b = 32;
nlist = round(logspace(3,6,10));

for t = [4]
for datatype = [1]
  rng(1)
  ntest = 10;
  err1  = zeros(length(nlist),1);
  err2  = zeros(length(nlist),1);
  err3  = zeros(length(nlist),1);
  for itest = 1:ntest
    for j = 1:length(nlist)
      n = nlist(j);
      fprintf('%g of %g nvals (n = %g), %g of %g repetitions\n',...
               j,length(nlist),n,itest,ntest)
      switch(datatype)
      case(1)
        x = rand(n,1); 
      case(2)
        x = (rand(n,1)-0.5)*2; 
      end
      x = chop(x,t); 

      strue = sum(x);
      s1    = summation_chop(x, t, 'blk', b);
      s2    = summation_chop(x, t, 'FABcomp', b);
      s3    = summation_chop(x, t, 'FABext', b);

      sumabsx = sum(abs(x));
      err1(j) = max(err1(j), abs(strue-s1)/sumabsx);
      err2(j) = max(err2(j), abs(strue-s2)/sumabsx);
      err3(j) = max(err3(j), abs(strue-s3)/sumabsx);
    end
  end

  figure()
  fs=14; ms=7; lw=1.5;
  loglog(nlist,err1,'-o','Markersize',ms,'LineWidth',lw);
  hold on;
  loglog(nlist,err2,'-x','Markersize',ms,'LineWidth',lw);
  loglog(nlist,err3,'-v','Markersize',ms,'LineWidth',lw);
  hleg = legend('Blocked','FAB compensated','FAB extended $$(u_e=2^{-24})$$');
  set(hleg,'Location','NorthWest','Fontsize',fs);
  if t==4 || t==8
    set(hleg,'Location','East','Fontsize',fs);
  else
    set(hleg,'Location','NorthWest','Fontsize',fs);
  end
  set(hleg,'interpreter','latex');
  xlabel('$$n$$','interpreter','latex','fontsize',fs);
  set(gca, 'Fontsize', fs);
  set(gcf, 'Color', 'w');
  file = sprintf('inner_prod_chop%d_%d',t,datatype);
  save(file, 't', 'datatype', 'nlist', 'err1', 'err2', 'err3');
  path = sprintf('../paper/figs/%s.pdf',file);
  export_fig(path);
end
end
