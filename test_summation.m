% TEST_SUMMATION
% Generates Figure 5.1

clear all;
close all;

b = 128;
nlist = round(logspace(3,8,20));

for datatype = [1 2]
  rng(1)
  ntest = 10;
  err1  = zeros(length(nlist),1);
  err2  = zeros(length(nlist),1);
  err3  = zeros(length(nlist),1);
  for itest = 1:ntest
    for j = 1:length(nlist)
      n = nlist(j);
      fprintf('%g of %g nvals (n = %g), %g of %g repetitions\n',...
               j,length(nlist),n,itest,ntest)
      switch(datatype)
      case(1)
        x = rand(n,1); 
      case(2)
        x = (rand(n,1)-0.5)*2; 
      end
      x = single(x); 

      strue = sum(double(x));
      s1    = double(summation(x,'blk',b));
      s2    = double(summation(x,'comp',b));
      s3    = double(summation(x,'FABcomp',b));
      sumabsx = sum(abs(double(x)));
      err1(j) = max(err1(j), abs(strue-s1)/sumabsx);
      err2(j) = max(err2(j), abs(strue-s2)/sumabsx);
      err3(j) = max(err3(j), abs(strue-s3)/sumabsx);
    end
  end

  fs=14; ms=7; lw=1.5;
  figure()
  loglog(nlist,err1,'-o','Markersize',ms,'LineWidth',lw);
  hold on;
  loglog(nlist,err2,'-x','Markersize',ms,'LineWidth',lw);
  loglog(nlist,err3,'-v','Markersize',ms,'LineWidth',lw);
  hleg = legend('Blocked','Compensated','FAB');
  if datatype==1
    set(hleg,'Location','NorthWest','Fontsize',fs);
  else
    set(hleg,'Location','SouthWest','Fontsize',fs);
  end
  xlim([1e3 1e8]);
  xticks([1e3 1e4 1e5 1e6 1e7 1e8]);
  set(hleg,'interpreter','latex');
  xlabel('$$n$$','interpreter','latex','fontsize',fs);
  set(gca, 'Fontsize', fs);
  set(gcf, 'Color', 'w');
end
