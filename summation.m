% SUMMATION
% Computes sum(x) with several summation algorithms
% Inputs:
%    x    = input vector, expected in single precision
%    summ = summation algorithm to be used. Possible values:
%           'rec'      => recursive summation
%           'blk'      => blocked summation
%           'comp'     => Kahan's compensated summation
%           'ext'      => extended precision summation
%           'FABcomp'  => FABsum algorithm with compensated summation as AccurateSum
%           'FABext'   => FABsum algorithm with double precision summation as AccurateSum
%    b    = block size (optional argument for FABsum)
% Outputs:
%    s    = computed sum
function s = summation(x,summ,b)
  n = length(x);
  switch(summ)
  case 'rec'
    s = 0;
    for i=1:n
      s = s + x(i);
    end
  case 'comp'
    s = 0; e = 0;
    for i=1:n
      temp = s;
      y    = e + x(i);
      s    = temp + y;
      e    = (temp - s) + y;
    end
  case 'blk'
    npart = ceil(n/b);
    s = 0; 
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      s = s + summation(x(ibeg:iend),'rec');
    end
  case 'ext'
    s = 0;
    for i=1:n
      s = s + double(x(i));
    end
    s = single(s);
  case 'FABcomp'
    npart = ceil(n/b);
    s = 0; e = 0;
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      temp = s;
      y    = e + summation(x(ibeg:iend),'rec');
      s    = temp + y;
      e    = (temp - s) + y;
    end
  case 'FABext'
    npart = ceil(n/b);
    s = 0; 
    for k = 1:npart
      ibeg = 1+(k-1)*b;
      iend = min(k*b,n);
      s    = s + double(summation(x(ibeg:iend)));
    end
    s = single(s);
  otherwise
    error('Wrong summ argument\n')
  end
end
