# FABsum

MATLAB codes to perform the numerical experiments reported in the article 
"A Class of Fast and Accurate Summation Algorithms" by P. Blanchard, N. J. Higham, and T. Mary.

## Included MATLAB files
* **summation.m**: implements several summation algorithms, including FABsum (Algorithm 3.1 in the paper).
* **summation_chop.m**: implements several summation algorithms in lower precisions, simulated with the **chop.m** function of
  "Simulating low precision floating-point arithmetic" by Higham and Pranesh.
* **test_summation.m**: generates Figure 5.1 of the paper.
* **test_summation_chop.m**: generates Figure 5.2 of the paper.
* **test_harmonic.m**: generates Figure 5.3 of the paper.

## Requirements
* The codes have been developed and tested with MATLAB 2018b.
* The scripts **summation_chop.m**, **test_summation_chop.m**, and
  **test_harmonic.m** require the function **chop.m** of "Simulating low
  precision floating-point arithmetic" by Higham and Pranesh.

## Experiments with PLASMA
Figures 5.4 and 5.5 of the paper are obtained with our modified version of the PLASMA library, which we have also
made available in the following repository: https://gitlab.com/nla-grp/plasma-17.1-xp
